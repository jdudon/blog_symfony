<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Article;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use App\Form\ArticleType;



class ArticleController extends Controller
{
    /**
     * @Route("/article", name="article")
     */
    public function index(ArticleRepository $repo)
    {
        $result = $repo->getAll();
        return $this->render('article/index.html.twig', [
            'controller_name' => 'ArticleController',
            'result' => $result
        ]);
    }

    
    /**
     * @Route("/article/{id}", name="show_single_article")
     */

    public function showArticle(int $id , ArticleRepository $repo)
    {
        $article = $repo->getById($id);

        return $this->render('article/show_single_article.html.twig', [
            "article" => $article
        ]);
    }




    /**
     * @Route("/admin/profile", name="profile")
     */
    public function manageProfile() 
    {
        return $this->render('admin_profile/index.html.twig', [
            "article" => $article
        ]);
    }

    /**
     * @Route("/admin/manage_article/{id}", name="manage_single_article")
     */


    public function articleById(int $id , ArticleRepository $repo, Request $request)
    {
        $article = $repo->getById($id);

        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $repo->update($form->getData());
            return $this->redirectToRoute("home");
        }
        

        return $this->render('manage_article/manage_single_article.html.twig', [
            "form" => $form->createView(),
            "article" => $article
        ]);
    }

        /**
     * @Route("/admin/article/remove/{id}", name="remove_article")
     */
    public function remove(int $id, ArticleRepository $repo) {
        $repo->delete($id);
        return $this->redirectToRoute("home");
    }

    /**
     * @Route("/admin/manage_article", name="manage_article")
     */

    public function manage_article(ArticleRepository $repo)
    { 

        
        $result = $repo->getAll();
        return $this->render('manage_article/index.html.twig', [
            'controller_name' => 'ManageArticleController',
            'result'=> $result,
        ]);
    }

    /**
     * @Route("admin/delete_article/{id}", name="delete")
     */
    public function delete(ArticleRepository $repo) 
    {
        $article = $repo->getById(row.id);
        $article->delete($id);
        return $this->render('manage_article/index.html.twig', [
            'controller_name' => 'ManageArticleController',
            'result'=> $result,
            'article'=>$article
            ]);
    }

        /**
     * @Route("/admin/article_creation", name="create")
     */
    public function article_creation(Request $request, ArticleRepository $repo)
    {
       
        $form = $this->createForm(ArticleType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $repo->add($form->getData());

            // $article->url->move(dirname(__FILE__)."/../../public/upload", "exemple.jpg");
            // $article->url = "upl/exemple.jpg";
            // $repo->add($article);
            return $this->redirectToRoute("home");
    }    

        return $this->render('article_creation/index.html.twig', [
            'form' => $form->createView(), 
            // 'article'=>$article
        ]);
    }
}

