<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;

class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function index(ArticleRepository $repo)
    {

        $result = $repo->getAll();
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'result' => $result
        ]);
    }
}
