<?php

namespace App\Repository;

use App\Entity\User;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;


class UserRepository implements UserProviderInterface
{

    private $connection;


    public function __construct()
    {
        try {

            $this->connection = new \PDO(
                "mysql:host=" . getenv("MYSQL_HOST") . ":3306;dbname=" . getenv("MYSQL_DATABASE"),
                getenv("MYSQL_USER"),
                getenv("MYSQL_PASSWORD")
            );

            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        } catch (\PDOException $e) {

            dump($e);

        }
    }

    private function fetch(string $query, array $params = []){
        try {

            $query = $this->connection->prepare($query);

            foreach ($params as $param => $value) {
                $query->bindValue($param, $value);
            }

            $query->execute();

            $result = [];

            foreach ($query->fetchAll() as $row) {

                $result[] = User::fromSQL($row);
            }

            if (count($result) <= 1) {
                return $result[0];
            }

            return $result;

        } catch (\PDOException $e) {
            dump($e);
        }

    }

    public function add(User $user)
    {

        $this->fetch("INSERT INTO user (blogName, name, surname, email, birthdate, password) VALUES (:blogName,:name, :surname, :email, :birthdate, :password)",[
            ":blogName" => $user->blogName,
            ":name" => $user->name,
            ":surname" => $user->surname,
            ":email" => $user->email,
            ":birthdate" => $user->birthdate,
            ":password" => $user->password
        ]);

        $user->id = intval($this->connection->lastInsertId());

        return $user;
    }

    public function get(int $id)
    {
        return $this->fetch("SELECT * FROM user WHERE :id", [":id" => $id]);
    }

    public function getAll()
    {
        return $this->fetch("SELECT * FROM user");
    }

    public function loadUserByUsername($username)
    {
        $user = $this->fetch("SELECT * FROM users WHERE email=:email", [
            ":email" => $username
        ]); 
        if($user instanceof User) {
            return $user;
        }
        throw new UsernameNotFoundException("User doesn't exist");
    }

    public function refreshUser(\Symfony\Component\Security\Core\User\UserInterface $user)
    {
        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return User::class === $class;
    }


}
