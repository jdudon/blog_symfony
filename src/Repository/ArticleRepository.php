<?php

namespace App\Repository;

use App\Entity\Article;


class ArticleRepository
{
    private $connection;


    public function __construct()
    {
        try {


            $this->connection = new \PDO(
                "mysql:host=" . getenv("MYSQL_HOST") . ":3306;dbname=" . getenv("MYSQL_DATABASE"),
                getenv("MYSQL_USER"),
                getenv("MYSQL_PASSWORD")
            );

            
            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        } catch (\PDOException $e) {

            dump($e);

        }
    }

    private function fetch(string $query, array $params = []){
        try {
            $query = $this->connection->prepare($query);
            foreach ($params as $param => $value) {
                $query->bindValue($param, $value);
            }

            $query->execute();

            $result = [];

            foreach ($query->fetchAll() as $row) {

                $result[] = Article::fromSQL($row);
            }

            if (count($result) <= 1) {
                return $result[0];
            }

            return $result;

        } catch (\PDOException $e) {
            dump($e);
        }

    }

    public function add(Article $article)
    {

        $this->fetch("INSERT INTO articles (title, tag, content) VALUES (:title, :tag, :content)",[
            ":title" => $article->title,
            ":tag" => $article->tag,
            ":content" => $article->content,

        ]);
        
        $article->id = intval($this->connection->lastInsertId());
        return $article;
    }

    public function getById(int $id): ?Article
    {
        return $this->fetch("SELECT * FROM articles WHERE id=:id", [":id" => $id]);
    }

    public function getAll()
    {
        return $this->fetch("SELECT * FROM articles");
    }
    
    public function update(Article $article) {
        try {

            $query = $this->connection->prepare("UPDATE articles SET title=:title, content=:content, tag=:tag WHERE id=:id");
            
            $query->bindValue(":title", $article->title);
            $query->bindValue(":content", $article->content);
            $query->bindValue(":tag", $article->tag);
            $query->bindValue(":id", $article->id);

            return $query->execute();

        } catch (\PDOException $e) {
            dump($e);
        }
        return false;
    }

    public function delete(int $id) {
        try {

            $query = $this->connection->prepare("DELETE FROM articles WHERE id=:id");
            
            $query->bindValue(":id", $id);

            return $query->execute();

        } catch (\PDOException $e) {
            dump($e);
        }
        return false;
    }

}

