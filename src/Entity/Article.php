<?php

namespace App\Entity;


class Article
{

    public $title;
    public $content;
    public $tag;
    public $id;

    public function __construct(string $title = null, string $content = null, string $tag = null, int $id = null) {

        $this->title = $title;
        $this->content = $content;
        $this->tag = $tag;
        $this->id = $id;

    }
  
    public static function fromSQL(array $rawData) {
        return new Article(

            $rawData["title"],
            $rawData["content"],
            $rawData["tag"],
            $rawData["id"]
        );
    }

}
