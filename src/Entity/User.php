<?php

namespace App\Entity;


use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;




class User implements UserInterface
{
    public $id;
    public $blogName;
    public $name;
    public $surname;
    public $email;
    public $birthdate;
    /**
    * @Assert\Length(min=5)
    */

    public $password;

    public function __construct(string $blogName = null, string $name = null, string $surname = null, string $email = null, \DateTime $birthdate = null,string $password, int $id = null) {
        $this->id = $id;
        $this->blogName = $blogName;
        $this->name = $name;
        $this->surname = $surname;
        $this->email = $email;
        $this->birthdate = $birthdate;
        $this->password = $password;
    }

    public static function fromSQL(array $rawData) {
        return new User(
            $rawData["blogName"],
            $rawData["name"],
            $rawData["surname"],
            $rawData["email"],
            new \DateTime($rawData["birthdate"]),
            $rawData["password"],
            $rawData["id"]
        );
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRoles()
    {
        return ["ROLE_ADMIN"];
    }

    public function getSalt()
    {
        
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function eraseCredentials()
    {
        
    }


}
